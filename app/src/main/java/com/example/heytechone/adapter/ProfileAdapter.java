package com.example.heytechone.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.heytechone.R;
import com.example.heytechone.fragment.models.Facebook;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ProfileAdapter  extends RecyclerView.Adapter<ProfileAdapter.MyViewHolder>{
    private Context context;
    private List<Facebook> data;

    public ProfileAdapter(List<Facebook> data, Context context) {
        this.data = data;
        this.context = context;
    }
    public static class MyViewHolder extends RecyclerView.ViewHolder{

        TextView ads_id;
        TextView total_ads;
        TextView boost_value;
        TextView ads_left;

         public MyViewHolder(@NonNull View itemView) {
             super(itemView);
             this.ads_id = itemView.findViewById(R.id.ads_id);
             this.total_ads = itemView.findViewById(R.id.total_ads);
             this.boost_value = itemView.findViewById(R.id.boost_value);
             this.ads_left= itemView.findViewById(R.id.ads_left);
         }
     }
    @NonNull
    @Override
    public ProfileAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.listitem , parent , false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProfileAdapter.MyViewHolder holder, int position) {
        Facebook fb = data.get(position);
        holder.ads_id.setText(fb.getAdsId());
        holder.total_ads.setText(fb.getTotalAds());
        holder.boost_value.setText(fb.getBoostValue());
        holder.ads_left.setText(fb.getAdsLeft());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
