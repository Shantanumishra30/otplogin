package com.example.heytechone.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Adapter;
import android.widget.TextView;

import com.example.heytechone.R;
import com.example.heytechone.adapter.ProfileAdapter;
import com.example.heytechone.fragment.models.FacebookList;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

public class ProfileActivity extends AppCompatActivity {

    //private RewardedVideoAd mAd;
    TextView rewardText;
    RecyclerView rv;
    RecyclerView.LayoutManager layoutManager;
    Adapter adapter;
    FacebookList facebookList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
      // MobileAds.initialize(this , "ca-app-pub-3940256099942544~3347511713");
        rewardText = findViewById(R.id.currentReward);
        rv = findViewById(R.id.rv);
        rv.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        rv.setLayoutManager(layoutManager);
        rv.setItemAnimator(new DefaultItemAnimator());

        facebookList = new Gson().fromJson(parseJSONData(), FacebookList.class );
        adapter = (Adapter) new ProfileAdapter(facebookList.getFacebookList(), this);

        rv.setAdapter((RecyclerView.Adapter) adapter);

//        mAd = MobileAds.getRewardedVideoAdInstance(this);
//        mAd.setRewardedVideoAdListener(this);
//        loadRewardedVideoAd();
    }

//    private void loadRewardedVideoAd() {
//        if(!mAd.isLoaded())
//        {
//            mAd.loadAd("ca-app-pub-3940256099942544/5224354917" ,new AdRequest.Builder().build());
//        }
//    }


//    @Override
//    public void onRewardedVideoAdLoaded() {
//
//    }
//
//    @Override
//    public void onRewardedVideoAdOpened() {
//
//    }
//
//    @Override
//    public void onRewardedVideoStarted() {
//
//    }
//
//    @Override
//    public void onRewardedVideoAdClosed() {
//        loadRewardedVideoAd();
//    }
//
//    @Override
//    public void onRewarded(RewardItem rewardItem) {
//        rewardText.setText("Current Rewards =15");
//    }
//
//    @Override
//    public void onRewardedVideoAdLeftApplication() {
//
//    }
//
//    @Override
//    public void onRewardedVideoAdFailedToLoad(int i) {
//
//    }
//
//    @Override
//    public void onRewardedVideoCompleted() {
//
//    }
//
//    public void GetReward(View view) {
//        if(mAd.isLoaded())
//        {
//            mAd.show();
//        }
//    }
//
//    @Override
//    protected void onPause() {
//        mAd.pause(this);
//        super.onPause();
//    }
//    @Override
//    protected void onDestroy() {
//        mAd.destroy(this);
//        super.onDestroy();
//    }
//    @Override
//    protected void onResume() {
//        mAd.resume(this);
//        super.onResume();
//    }

    public String parseJSONData() {
        String JSONString = null;
        JSONObject JSONObject = null;
        try {

            InputStream inputStream = getAssets().open("userDetail.json");

            int sizeOfJSONFile = inputStream.available();

            byte[] bytes = new byte[sizeOfJSONFile];

            inputStream.read(bytes);

            inputStream.close();

            JSONString = new String(bytes, "UTF-8");

        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return JSONString;
    }
    public void heyTechButton(View view) {

    }
}