
package com.example.heytechone.fragment.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Rewards {

    @SerializedName("facebook")
    @Expose
    private Facebook facebook;
    @SerializedName("heycalls")
    @Expose
    private Heycalls heycalls;
    @SerializedName("google")
    @Expose
    private Google google;

    public Facebook getFacebook() {
        return facebook;
    }

    public void setFacebook(Facebook facebook) {
        this.facebook = facebook;
    }

    public Heycalls getHeycalls() {
        return heycalls;
    }

    public void setHeycalls(Heycalls heycalls) {
        this.heycalls = heycalls;
    }

    public Google getGoogle() {
        return google;
    }

    public void setGoogle(Google google) {
        this.google = google;
    }

}
