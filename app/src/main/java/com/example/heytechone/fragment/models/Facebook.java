
package com.example.heytechone.fragment.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Facebook {

    @SerializedName("ads_id")
    @Expose
    private String adsId;
    @SerializedName("total_ads")
    @Expose
    private String totalAds;
    @SerializedName("boost_value")
    @Expose
    private String boostValue;
    @SerializedName("ads_left")
    @Expose
    private String adsLeft;
    @SerializedName("life")
    @Expose
    private Life life;
    @SerializedName("total_fb_coin")
    @Expose
    private TotalFbCoin totalFbCoin;
    @SerializedName("fb_coin_earn")
    @Expose
    private FbCoinEarn fbCoinEarn;

    public String getAdsId() {
        return adsId;
    }

    public void setAdsId(String adsId) {
        this.adsId = adsId;
    }

    public String getTotalAds() {
        return totalAds;
    }

    public void setTotalAds(String totalAds) {
        this.totalAds = totalAds;
    }

    public String getBoostValue() {
        return boostValue;
    }

    public void setBoostValue(String boostValue) {
        this.boostValue = boostValue;
    }

    public String getAdsLeft() {
        return adsLeft;
    }

    public void setAdsLeft(String adsLeft) {
        this.adsLeft = adsLeft;
    }

    public Life getLife() {
        return life;
    }

    public void setLife(Life life) {
        this.life = life;
    }

    public TotalFbCoin getTotalFbCoin() {
        return totalFbCoin;
    }

    public void setTotalFbCoin(TotalFbCoin totalFbCoin) {
        this.totalFbCoin = totalFbCoin;
    }

    public FbCoinEarn getFbCoinEarn() {
        return fbCoinEarn;
    }

    public void setFbCoinEarn(FbCoinEarn fbCoinEarn) {
        this.fbCoinEarn = fbCoinEarn;
    }

}
