package com.example.heytechone.fragment.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FacebookList {

    @SerializedName("Details")
    @Expose
    private List<Facebook> list = null;

    public List<Facebook> getFacebookList() {
        return list;
    }

    public void setFacebookList(List<Facebook> detailList) {
        this.list = detailList;
    }

}