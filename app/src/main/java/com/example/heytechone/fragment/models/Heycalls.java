
package com.example.heytechone.fragment.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Heycalls {

    @SerializedName("ads_id")
    @Expose
    private String adsId;
    @SerializedName("total_ads")
    @Expose
    private String totalAds;
    @SerializedName("boost_value")
    @Expose
    private String boostValue;
    @SerializedName("ads_left")
    @Expose
    private String adsLeft;
    @SerializedName("life")
    @Expose
    private Life_ life;
    @SerializedName("total_hc_coin")
    @Expose
    private TotalHcCoin totalHcCoin;
    @SerializedName("hc_coin_earn")
    @Expose
    private HcCoinEarn hcCoinEarn;

    public String getAdsId() {
        return adsId;
    }

    public void setAdsId(String adsId) {
        this.adsId = adsId;
    }

    public String getTotalAds() {
        return totalAds;
    }

    public void setTotalAds(String totalAds) {
        this.totalAds = totalAds;
    }

    public String getBoostValue() {
        return boostValue;
    }

    public void setBoostValue(String boostValue) {
        this.boostValue = boostValue;
    }

    public String getAdsLeft() {
        return adsLeft;
    }

    public void setAdsLeft(String adsLeft) {
        this.adsLeft = adsLeft;
    }

    public Life_ getLife() {
        return life;
    }

    public void setLife(Life_ life) {
        this.life = life;
    }

    public TotalHcCoin getTotalHcCoin() {
        return totalHcCoin;
    }

    public void setTotalHcCoin(TotalHcCoin totalHcCoin) {
        this.totalHcCoin = totalHcCoin;
    }

    public HcCoinEarn getHcCoinEarn() {
        return hcCoinEarn;
    }

    public void setHcCoinEarn(HcCoinEarn hcCoinEarn) {
        this.hcCoinEarn = hcCoinEarn;
    }

}
