
package com.example.heytechone.fragment.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Google {

    @SerializedName("ads_id")
    @Expose
    private String adsId;
    @SerializedName("total_ads")
    @Expose
    private String totalAds;
    @SerializedName("boost_value")
    @Expose
    private String boostValue;
    @SerializedName("ads_left")
    @Expose
    private String adsLeft;
    @SerializedName("life")
    @Expose
    private Life__ life;
    @SerializedName("total_gg_coin")
    @Expose
    private TotalGgCoin totalGgCoin;
    @SerializedName("gg_coin_earn")
    @Expose
    private GgCoinEarn ggCoinEarn;

    public String getAdsId() {
        return adsId;
    }

    public void setAdsId(String adsId) {
        this.adsId = adsId;
    }

    public String getTotalAds() {
        return totalAds;
    }

    public void setTotalAds(String totalAds) {
        this.totalAds = totalAds;
    }

    public String getBoostValue() {
        return boostValue;
    }

    public void setBoostValue(String boostValue) {
        this.boostValue = boostValue;
    }

    public String getAdsLeft() {
        return adsLeft;
    }

    public void setAdsLeft(String adsLeft) {
        this.adsLeft = adsLeft;
    }

    public Life__ getLife() {
        return life;
    }

    public void setLife(Life__ life) {
        this.life = life;
    }

    public TotalGgCoin getTotalGgCoin() {
        return totalGgCoin;
    }

    public void setTotalGgCoin(TotalGgCoin totalGgCoin) {
        this.totalGgCoin = totalGgCoin;
    }

    public GgCoinEarn getGgCoinEarn() {
        return ggCoinEarn;
    }

    public void setGgCoinEarn(GgCoinEarn ggCoinEarn) {
        this.ggCoinEarn = ggCoinEarn;
    }

}
